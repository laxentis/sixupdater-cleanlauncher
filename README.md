SixUpdater-CleanLauncher
========================
Disclaimer
----------
This software is provided by its author "as is" and "with all faults."
The author makes no representations or warranties of any kind concerning the safety, suitability, lack of viruses, inaccuracies, typographical errors, or other harmful components of this product.
There are inherent dangers in the use of any software, and you are solely responsible for determining whether this one is compatible with your equipment and other software installed on your equipment.
You are also solely responsible for the protection of your equipment and backup of your data, and the author will not be liable for any damages you may suffer in connection with using, modifying, or distributing this software.
Download
--------
https://github.com/laxentis/SixUpdater-CleanLauncher/blob/master/SixUpdater-CleanLauncher/bin/Release/SixUpdater-CleanLauncher.exe?raw=true
